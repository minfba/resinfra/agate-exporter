package de.uniba.minf.agate.exporter.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tx_academy_domain_model_persons")
public class Person {
	@Id
	@Column(name="uid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
		
	@Column(name="persistent_identifier")
	private String persistentIdentifier;

	@Column(name="given_name")
	private String givenName;
	
	@Column(name="additional_name")
	private String additionalName;
	
	@Column(name="family_name")
	private String familyName;
	
	@Column(name="honorific_prefix")
	private String prefix;
	
	@Column(name="honorific_suffix")
	private String suffix;
		
	private int gender;
}
