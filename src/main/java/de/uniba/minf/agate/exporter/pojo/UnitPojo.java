package de.uniba.minf.agate.exporter.pojo;

import de.uniba.minf.agate.exporter.pojo.base.BaseLocalizedPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class UnitPojo extends BaseLocalizedPojo {
	
}
