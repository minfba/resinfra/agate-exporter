package de.uniba.minf.agate.exporter.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.uniba.minf.agate.exporter.model.base.BaseLocalized;
import de.uniba.minf.agate.exporter.pojo.LocalizedDescriptionPojo;
import de.uniba.minf.agate.exporter.pojo.base.BaseLocalizedPojo;

public abstract class BaseLocalizedPojoService<T extends BaseLocalized, P extends BaseLocalizedPojo> {

	public List<P> findAll() {
		List<P> pojos = new ArrayList<>();
		P pojo;
		for (T obj : this.findAllOrig()) {
			pojo = this.createPojo(obj);
			pojo.addIdIfNotContained(obj.getId());
			pojos.add(pojo);
		}
		this.appendLocalizedDescriptions(pojos);	
		return pojos;
	}
	
	protected void appendLocalizedDescriptions(List<P> pojos) {
		P pojo = null;
		for (T uNonOrig : this.findAllNonOrig()) {
			for (P exPojo : pojos) {
				if (exPojo.getIds().contains(uNonOrig.getOrigId())) {
					pojo = exPojo;
					break;
				}
			}			
			if (pojo==null) { 
				pojo = this.createPojo(uNonOrig);
				// This is acually done because the hierarchies in the database are inconsistent and we need to cope with that by collecting ids
				pojo.addIdIfNotContained(uNonOrig.getOrigId());
				pojo.addIdIfNotContained(uNonOrig.getId());
				pojos.add(pojo);
			} else {
				pojo.addIdIfNotContained(uNonOrig.getId());
				pojo.getDescriptions().add(new LocalizedDescriptionPojo(uNonOrig.getLanguageId(), uNonOrig.getTitle(), uNonOrig.getDescription(), uNonOrig.getAcronym(), uNonOrig.getSubtitle()));
			}
		}
	}
	
	protected void fillPojo(P pojo, T obj) {
		pojo.addIdIfNotContained(obj.getId());
		pojo.setPersistentIdentifier(obj.getPersistentIdentifier());
		pojo.getDescriptions().add(new LocalizedDescriptionPojo(obj.getLanguageId(), obj.getTitle(), obj.getDescription(), obj.getAcronym(), obj.getSubtitle()));
	}
	
	
	protected abstract Set<T> findAllOrig();
	protected abstract Set<T> findAllNonOrig();
	
	protected abstract P createPojo(T obj);
}
