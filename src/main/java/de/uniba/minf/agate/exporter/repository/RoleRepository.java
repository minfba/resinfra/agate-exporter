package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

	@Query("SELECT r FROM Role r WHERE r.origId = 0")
	public Set<Role> findAllOrig();
	
	@Query("SELECT r FROM Role r WHERE r.origId != 0")
	public Set<Role> findAllNonOrig();
}
