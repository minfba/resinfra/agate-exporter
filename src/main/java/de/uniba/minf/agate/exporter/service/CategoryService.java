package de.uniba.minf.agate.exporter.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.uniba.minf.agate.exporter.model.Category;
import de.uniba.minf.agate.exporter.pojo.CategoryPojo;
import de.uniba.minf.agate.exporter.repository.CategoryRepository;
import de.uniba.minf.agate.exporter.service.base.BaseLocalizedPojoService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CategoryService extends BaseLocalizedPojoService<Category, CategoryPojo> {
	@Autowired private CategoryRepository categoryRepository;

	@Override
	protected Set<Category> findAllOrig() {
		return categoryRepository.findAllOrig();
	}

	@Override
	protected Set<Category> findAllNonOrig() {
		return categoryRepository.findAllNonOrig();
	}

	@Override
	protected CategoryPojo createPojo(Category obj) {
		CategoryPojo pojo = new CategoryPojo();
		super.fillPojo(pojo, obj);
		return pojo;
	}
	
	@Override
	public List<CategoryPojo> findAll() {
		List<CategoryPojo> categoryPojos = new ArrayList<>();
		List<CategoryPojo> rootCategoryPojos = new ArrayList<>();
		
		this.fillCategoryHierarchy(categoryPojos, rootCategoryPojos);
		return categoryPojos;
	}	
	
	public Collection<CategoryPojo> findAllRoot() {
		List<CategoryPojo> categoryPojos = new ArrayList<>();
		List<CategoryPojo> rootCategoryPojos = new ArrayList<>();
		
		this.fillCategoryHierarchy(categoryPojos, rootCategoryPojos);
		return rootCategoryPojos;
	}
	
	
	private void fillCategoryHierarchy(List<CategoryPojo> categoryPojos, List<CategoryPojo> rootCategoryPojos) {
		CategoryPojo pojo;
		for (Category cOrig : categoryRepository.findAllRoot()) {
			pojo = this.createPojo(cOrig);
			pojo.addIdIfNotContained(cOrig.getId());
			rootCategoryPojos.add(pojo);
			categoryPojos.add(pojo);
		}
		
		Set<Category> nonRootCategories = categoryRepository.findAllNonRoot();
		
		// This is to protect from endless loops if hierarchy is inconsistent
		int loopCount = 0;
		Collection<Category> processedNonRoot;
		while (!nonRootCategories.isEmpty()) {
			if (loopCount>=100) {
				log.warn("Cancelled hierarchy combination to prevent endless loop; hierarchy inconsistency");
				break;
			}			
			loopCount++;
			processedNonRoot = new ArrayList<>();
			for (Category cNonRoot : nonRootCategories) {
				for (CategoryPojo exPojo : categoryPojos) {
					if (exPojo.getIds().contains(cNonRoot.getParent())) {
						pojo = this.createPojo(cNonRoot);
						pojo.addIdIfNotContained(cNonRoot.getId());
						pojo.setParent(exPojo);
						exPojo.getChildren().add(pojo);
						categoryPojos.add(pojo);
						processedNonRoot.add(cNonRoot);
						break;
					}
				}
			}
			nonRootCategories.removeAll(processedNonRoot);
		}

		this.appendLocalizedDescriptions(categoryPojos);
	}

	public CategoryPojo getRoot(CategoryPojo cPojo) {
		// Return if there is no parent (==root) or if the remaining parent is "Project Data" (374) or "Digital Humanities" (370)
		if (cPojo.getParent()==null || cPojo.getParent().getIds().contains(374) || cPojo.getParent().getIds().contains(370)) {
			return cPojo;
		} else {
			return this.getRoot(cPojo.getParent());
		}
	}

	public String getMainLabel(CategoryPojo cPojo) {
		if (cPojo.getDescriptions().isEmpty()) {
			return "undefined";
		} else {
			return cPojo.getDescriptions().get(0).getTitle();
		}
	}
}
