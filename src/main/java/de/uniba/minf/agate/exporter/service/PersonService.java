package de.uniba.minf.agate.exporter.service;

import org.springframework.stereotype.Component;

import de.uniba.minf.agate.exporter.model.Person;
import de.uniba.minf.agate.exporter.pojo.PersonPojo;
import de.uniba.minf.agate.exporter.pojo.PersonPojo.GENDER;

@Component
public class PersonService {
	public PersonPojo createPersonPojo(Person person) {
		PersonPojo pPojo = new PersonPojo();
		pPojo.setId(person.getId());
		pPojo.setAdditionalName(person.getAdditionalName());
		pPojo.setFamilyName(person.getFamilyName());
		pPojo.setGender(person.getGender()==1 ? GENDER.FEMALE : GENDER.MALE);
		pPojo.setGivenName(person.getGivenName());
		pPojo.setPrefix(person.getPrefix());
		pPojo.setSuffix(person.getSuffix());
		return pPojo;
	}
}