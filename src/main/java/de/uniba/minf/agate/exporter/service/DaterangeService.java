package de.uniba.minf.agate.exporter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.uniba.minf.agate.exporter.model.Daterange;
import de.uniba.minf.agate.exporter.pojo.DaterangePojo;
import de.uniba.minf.agate.exporter.repository.DaterangeRepository;

@Component
public class DaterangeService {
	
	@Autowired private DaterangeRepository daterangeRepository;
	
	public List<DaterangePojo> findAllForProjects() {
		return daterangeRepository.findAllForProjects().stream().map(dr -> this.createPersonPojo(dr)).toList();
	} 
	
	public DaterangePojo createPersonPojo(Daterange daterange) {
		DaterangePojo pojo = new DaterangePojo();
		pojo.setLabel(daterange.getLabel());
		pojo.setFrom(daterange.getFrom());
		pojo.setTo(daterange.getTo());
		pojo.setId(daterange.getId());
		pojo.setParent(daterange.getParent());
		return pojo;
	}
}
