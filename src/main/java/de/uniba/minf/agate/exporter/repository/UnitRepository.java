package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Unit;

public interface UnitRepository extends CrudRepository<Unit, Integer> {

	@Query("SELECT u FROM Unit u WHERE u.origId = 0")
	public Set<Unit> findAllOrig();
	
	@Query("SELECT u FROM Unit u WHERE u.origId != 0")
	public Set<Unit> findAllNonOrig();
}
