package de.uniba.minf.agate.exporter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgateExporterApplication {
	public static void main(String[] args) {
		SpringApplication.run(AgateExporterApplication.class, args);
	}
}