package de.uniba.minf.agate.exporter.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tx_chftime_domain_model_dateranges")
public class Daterange {
	@Id
	@Column(name="uid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
		
	private String label;
	
	@Column(name = "dating_from")
	private String from;
	
	@Column(name = "dating_to")
	private String to;
	
	private int parent;
	private int deleted;
	private String tablename;
}
