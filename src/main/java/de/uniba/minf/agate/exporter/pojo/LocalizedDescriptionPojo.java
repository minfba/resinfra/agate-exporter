package de.uniba.minf.agate.exporter.pojo;

import lombok.Data;

@Data
public class LocalizedDescriptionPojo {
	private String lang;
	private String title;
	private String description;	
	private String acronym;
	private String subtitle;
	
	public LocalizedDescriptionPojo(int languageId, String title, String description, String acronym, String subtitle) {
		if (languageId==1) {
			this.lang = "deu";
		} else if (languageId==2) {
			this.lang = "fra";
		} else {
			this.lang =  "eng";
		}
		
		this.title = title;
		this.description = description;
		this.acronym = acronym;
		this.subtitle = subtitle;
	}
}
