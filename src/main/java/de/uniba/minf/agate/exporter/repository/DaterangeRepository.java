package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Daterange;

public interface DaterangeRepository extends CrudRepository<Daterange, Integer> {
	@Query("SELECT d FROM Daterange d WHERE d.tablename = 'tx_academy_domain_model_projects' AND d.deleted = 0")
	public Set<Daterange> findAllForProjects();
	
	@Query("SELECT d FROM Daterange d WHERE d.tablename = 'tx_academy_domain_model_relations' AND d.deleted = 0")
	public Set<Daterange> findAllForRelations();
}
