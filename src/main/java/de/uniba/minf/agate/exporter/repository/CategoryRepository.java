package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
	@Query("SELECT c FROM Category c WHERE c.parent = 0 AND c.origId = 0")
	public Set<Category> findAllRoot();
	
	@Query("SELECT c FROM Category c WHERE c.parent != 0 AND c.origId = 0")
	public Set<Category> findAllNonRoot();
	
	@Query("SELECT c FROM Category c WHERE c.origId = 0")
	public Set<Category> findAllOrig();
	
	@Query("SELECT c FROM Category c WHERE c.origId != 0")
	public Set<Category> findAllNonOrig();
}
