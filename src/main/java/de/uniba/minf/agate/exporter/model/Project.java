package de.uniba.minf.agate.exporter.model;

import java.util.Set;

import de.uniba.minf.agate.exporter.model.base.BaseLocalized;
import jakarta.persistence.OneToMany;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "tx_academy_domain_model_projects")
public class Project extends BaseLocalized {
	private String description;
	private String identifier;
	
	@Column(name="external_identifier")
	private String externalIdentifier;
	
	private String acronym;
	private String subtitle;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "sys_category_record_mm", joinColumns = @JoinColumn(name = "uid_foreign"), inverseJoinColumns = @JoinColumn(name = "uid_local"))
	private Set<Category> categories;
	
	@OneToMany(mappedBy="project")
	private Set<Relation> relations;
}
