package de.uniba.minf.agate.exporter.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.uniba.minf.agate.exporter.model.Category;
import de.uniba.minf.agate.exporter.model.Project;
import de.uniba.minf.agate.exporter.model.Relation;
import de.uniba.minf.agate.exporter.model.Url;
import de.uniba.minf.agate.exporter.pojo.CategoryPojo;
import de.uniba.minf.agate.exporter.pojo.DaterangePojo;
import de.uniba.minf.agate.exporter.pojo.PersonPojo;
import de.uniba.minf.agate.exporter.pojo.ProjectPojo;
import de.uniba.minf.agate.exporter.pojo.RolePojo;
import de.uniba.minf.agate.exporter.pojo.UnitPojo;
import de.uniba.minf.agate.exporter.repository.ProjectRepository;
import de.uniba.minf.agate.exporter.service.base.BaseLocalizedPojoService;

@Component
public class ProjectService extends BaseLocalizedPojoService<Project, ProjectPojo> {

	@Autowired private ProjectRepository projectRepository;
	@Autowired private DaterangeService daterangeService;
	
	@Autowired private CategoryService categoryService;
	@Autowired private PersonService personService;
	@Autowired private RoleService roleService;
	@Autowired private UnitService unitService;
	
	@Autowired private ObjectMapper objectMapper;
	
	
	@Override
	protected Set<Project> findAllOrig() {
		return projectRepository.findAllOrig();
	}

	@Override
	protected Set<Project> findAllNonOrig() {
		return projectRepository.findAllNonOrig();
	}

	@Override
	protected ProjectPojo createPojo(Project obj) {
		ProjectPojo pojo = new ProjectPojo();
		super.fillPojo(pojo, obj);
		pojo.setExternalIdentifier(obj.getExternalIdentifier());
		pojo.setIdentifier(obj.getIdentifier());		
		return pojo;
	}
	
	@Override
	public List<ProjectPojo> findAll() {
		List<ProjectPojo> projectPojos = new ArrayList<>();
		Map<Integer, Project> projectIdProjectMap = new HashMap<>();
		
		ProjectPojo pojo;
		for (Project obj : projectRepository.findAllOrig()) {
			pojo = this.createPojo(obj);
			pojo.addIdIfNotContained(obj.getId());
			projectPojos.add(pojo);
			projectIdProjectMap.put(obj.getId(), obj);
		}
		
		this.appendLocalizedDescriptions(projectPojos);
		
		this.appendCategories(projectPojos, projectIdProjectMap);		
		this.appendRelations(projectPojos, projectIdProjectMap);
		this.appendDateranges(projectPojos);
		
		return projectPojos;
	}
	
	
	private void appendDateranges(List<ProjectPojo> projectPojos) {
		List<DaterangePojo> dateranges = daterangeService.findAllForProjects();		
		for (ProjectPojo pPojo : projectPojos) {
			for (DaterangePojo drPojo : dateranges) {
				if (pPojo.getIds().contains(drPojo.getParent())) {
					pPojo.getDateranges().add(drPojo);
				}
			}
		}
	}

	private void appendRelations(Collection<ProjectPojo> projectPojos, Map<Integer, Project> projectIdProjectMap) {
		List<RolePojo> roles = roleService.findAll();
		List<UnitPojo> units = unitService.findAll();
	
		Project p;	
		for (ProjectPojo pPojo : projectPojos) {
			for (int id : pPojo.getIds()) {
				p = projectIdProjectMap.get(id);
				// p==null for nonOrig
				if (p!=null && p.getRelations()!=null) {
					for (Relation rel : p.getRelations()) {
						if (rel.getPerson()!=null) {
							this.appendPerson(pPojo, rel, roles);
						} else if (rel.getUnit()!=null) {
							this.appendUnit(pPojo, rel, roles, units);
						} else if (rel.getHcard()!=null) {
							this.appendHcard(pPojo, rel, roles, units);
						}
					}
				}
			}
		}
	}
	
	private void appendPerson(ProjectPojo pPojo, Relation rel, List<RolePojo> roles) {
		ObjectNode personNode = objectMapper.createObjectNode();
		PersonPojo psPojo = personService.createPersonPojo(rel.getPerson());
		personNode.set("person", objectMapper.valueToTree(psPojo));
		if (rel.getRole()!=null) {
			for (RolePojo rPojo : roles) {
				if (rPojo.getIds().contains(rel.getRole().getId())) {
					personNode.set("role", objectMapper.valueToTree(rPojo));
					break;
				}
			}
		}
		pPojo.getPersons().add(personNode);
	}

	private void appendHcard(ProjectPojo pPojo, Relation rel, List<RolePojo> roles, List<UnitPojo> units) {
		List<String> urls = new ArrayList<>();
		
		if (rel.getHcard().getUrls()!=null && !rel.getHcard().getUrls().isEmpty()) {
			for (Url url : rel.getHcard().getUrls()) {
				urls.add(url.getValue());
			}
		} 
		
		pPojo.setUrls(urls);
	}
	
	private void appendUnit(ProjectPojo pPojo, Relation rel, List<RolePojo> roles, List<UnitPojo> units) {
		ObjectNode unitNode = objectMapper.createObjectNode();
		UnitPojo uPojo = null;
		for (UnitPojo exPojo : units) {
			if (exPojo.getIds().contains(rel.getUnit().getId())) {
				uPojo = exPojo;
				break;
			}
		}
		
		unitNode.set("unit", objectMapper.valueToTree(uPojo));
		if (rel.getRole()!=null) {
			for (RolePojo rPojo : roles) {
				if (rPojo.getIds().contains(rel.getRole().getId())) {
					unitNode.set("role", objectMapper.valueToTree(rPojo));
					break;
				}
			}
		}
		pPojo.getUnits().add(unitNode);
	}
	
	private void appendCategories(Collection<ProjectPojo> projectPojos, Map<Integer, Project> projectIdProjectMap) {
		List<CategoryPojo> categories = categoryService.findAll();
		Project p;
		Map<String, List<CategoryPojo>> rootPropertyCategoryPojoMap;
		
		CategoryPojo cPojo;
		String propertyLabel;
		
		for (ProjectPojo pPojo : projectPojos) {
			rootPropertyCategoryPojoMap = new HashMap<>();
			for (int id : pPojo.getIds()) {
				p = projectIdProjectMap.get(id);
				// p==null for nonOrig
				if (p==null) {
					continue;
				}
				for (Category c : p.getCategories()) {
					cPojo = null;
					for (CategoryPojo cHierarchical : categories) {
						if (cHierarchical.getIds().contains(c.getId())) {
							cPojo = cHierarchical;
							break;
						}
					}
					if (cPojo==null) {
						continue;
					}
					
					// No longer required in this transaction
					cPojo.setChildren(null);
					
					// Root categories serve as main label to put actual categories below
					propertyLabel = categoryService.getMainLabel(categoryService.getRoot(cPojo));
					if (rootPropertyCategoryPojoMap.containsKey(propertyLabel)) {
						rootPropertyCategoryPojoMap.get(propertyLabel).add(cPojo);
					} else {
						List<CategoryPojo> propertyPojoList = new ArrayList<>();
						propertyPojoList.add(cPojo);
						rootPropertyCategoryPojoMap.put(propertyLabel, propertyPojoList);
					}
				}			
			}
			
			ObjectNode categoryNode = objectMapper.createObjectNode();
			String key;
			for (Entry<String, List<CategoryPojo>> rootPropertyCategoryPojoEntry : rootPropertyCategoryPojoMap.entrySet()) {
				key = rootPropertyCategoryPojoEntry.getKey().replace(" ", "");
				key = key.substring(0, 1).toLowerCase() + key.substring(1);
				categoryNode.set(key, objectMapper.valueToTree(rootPropertyCategoryPojoEntry.getValue()));
			}
			
			pPojo.setCategories(categoryNode);
		}
	}	
}