package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Project;

public interface ProjectRepository extends CrudRepository<Project, Integer> {

	@Query("SELECT p FROM Project p WHERE p.origId = 0")
	public Set<Project> findAllOrig();
	
	@Query("SELECT p FROM Project p WHERE p.origId != 0")
	public Set<Project> findAllNonOrig();
}
