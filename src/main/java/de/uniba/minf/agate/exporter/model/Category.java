package de.uniba.minf.agate.exporter.model;

import de.uniba.minf.agate.exporter.model.base.BaseLocalized;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "sys_category")
public class Category extends BaseLocalized {

	private String description;
	
	private int parent;
	
	@Transient
	private Category parentCategory;

	@Override
	public String getAcronym() {
		return null;
	}
}
