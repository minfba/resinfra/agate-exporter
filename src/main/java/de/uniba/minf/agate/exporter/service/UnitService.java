package de.uniba.minf.agate.exporter.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.uniba.minf.agate.exporter.model.Unit;
import de.uniba.minf.agate.exporter.pojo.UnitPojo;
import de.uniba.minf.agate.exporter.repository.UnitRepository;
import de.uniba.minf.agate.exporter.service.base.BaseLocalizedPojoService;

@Component
public class UnitService extends BaseLocalizedPojoService<Unit, UnitPojo> {
	@Autowired private UnitRepository unitRepository;

	@Override
	protected Set<Unit> findAllOrig() {
		return unitRepository.findAllOrig();
	}

	@Override
	protected Set<Unit> findAllNonOrig() {
		return unitRepository.findAllNonOrig();
	}

	@Override
	protected UnitPojo createPojo(Unit obj) {
		UnitPojo pojo = new UnitPojo();
		super.fillPojo(pojo, obj);
		return pojo;
	}
}