package de.uniba.minf.agate.exporter.model;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tx_academy_domain_model_hcards")
public class Hcard {
	@Id
	@Column(name="uid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="persistent_identifier")
	private String persistentIdentifier;

	private String label;
	
	@OneToMany(mappedBy="parent")
	private Set<Url> urls;
}
