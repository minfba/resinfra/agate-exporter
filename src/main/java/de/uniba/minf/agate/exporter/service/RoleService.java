package de.uniba.minf.agate.exporter.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.uniba.minf.agate.exporter.model.Role;
import de.uniba.minf.agate.exporter.pojo.RolePojo;
import de.uniba.minf.agate.exporter.repository.RoleRepository;
import de.uniba.minf.agate.exporter.service.base.BaseLocalizedPojoService;

@Component
public class RoleService extends BaseLocalizedPojoService<Role, RolePojo> {
	@Autowired private RoleRepository roleRepository;

	@Override
	protected Set<Role> findAllOrig() {
		return roleRepository.findAllOrig();
	}

	@Override
	protected Set<Role> findAllNonOrig() {
		return roleRepository.findAllNonOrig();
	}

	@Override
	protected RolePojo createPojo(Role obj) {
		RolePojo pojo = new RolePojo();
		super.fillPojo(pojo, obj);
		return pojo;
	}
}