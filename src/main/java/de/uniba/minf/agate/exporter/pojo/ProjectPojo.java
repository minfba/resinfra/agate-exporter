package de.uniba.minf.agate.exporter.pojo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.uniba.minf.agate.exporter.pojo.base.BaseLocalizedPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProjectPojo extends BaseLocalizedPojo {	
	public enum PROJECT_TYPE { DATA_REPOSITORY, COLLECTION, DICTIONARY, EDITION }
	
	private PROJECT_TYPE type;
	private String identifier;
	private String externalIdentifier;	
	
	private List<String> urls;
	private List<DaterangePojo> dateranges = new ArrayList<>();
	
	private ObjectNode categories;
	private List<ObjectNode> persons = new ArrayList<>();
	private List<ObjectNode> units = new ArrayList<>();
}
