package de.uniba.minf.agate.exporter.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class DaterangePojo {
	@JsonIgnore
	private int id;
	
	@JsonIgnore
	private int parent;
	
	private String label;
	private String from;
	private String to;
}
