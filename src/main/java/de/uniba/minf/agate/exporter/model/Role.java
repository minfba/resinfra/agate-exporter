package de.uniba.minf.agate.exporter.model;

import de.uniba.minf.agate.exporter.model.base.BaseLocalized;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "tx_academy_domain_model_roles")
public class Role extends BaseLocalized {
	
	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public String getAcronym() {
		return null;
	}
}
