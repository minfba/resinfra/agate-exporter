package de.uniba.minf.agate.exporter.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseLocalized {
	@Id
	@Column(name="uid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="t3_origuid")
	private Integer origId;
	
	@JsonIgnore
	@Column(name="sys_language_uid")
	private Integer languageId;
		
	@Column(name="persistent_identifier")
	private String persistentIdentifier;
	
	private String title;
	
	public String getDescription() { return null; }
	public String getAcronym() { return null; }
	
	public String getSubtitle() { return null; }
}
