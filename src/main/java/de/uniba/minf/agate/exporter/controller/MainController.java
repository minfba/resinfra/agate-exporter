package de.uniba.minf.agate.exporter.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import de.uniba.minf.agate.exporter.model.Hcard;
import de.uniba.minf.agate.exporter.model.Project;
import de.uniba.minf.agate.exporter.model.Relation;
import de.uniba.minf.agate.exporter.pojo.CategoryPojo;
import de.uniba.minf.agate.exporter.pojo.ProjectPojo;
import de.uniba.minf.agate.exporter.pojo.RolePojo;
import de.uniba.minf.agate.exporter.pojo.UnitPojo;
import de.uniba.minf.agate.exporter.repository.HcardRepository;
import de.uniba.minf.agate.exporter.repository.ProjectRepository;
import de.uniba.minf.agate.exporter.repository.RelationRepository;
import de.uniba.minf.agate.exporter.service.CategoryService;
import de.uniba.minf.agate.exporter.service.ProjectService;
import de.uniba.minf.agate.exporter.service.RoleService;
import de.uniba.minf.agate.exporter.service.UnitService;

@Controller
@RequestMapping
public class MainController {
	@Autowired private ProjectService projectService;
	@Autowired private CategoryService categoryService;
	@Autowired private RoleService roleService;
	@Autowired private UnitService unitService;
	
	@Autowired private HcardRepository hcardRepository;
	
	@Autowired private ProjectRepository projectRepository;
	@Autowired private RelationRepository relationRepository;
	
	
	@GetMapping
	public @ResponseBody Collection<ProjectPojo> getAllCombined() {
		return projectService.findAll();
	}
	
	@GetMapping("/relations")
	public @ResponseBody Iterable<Relation> getAllRelations() {
		return relationRepository.findByProject(1);
	}
	
	@GetMapping("/projects")
	public @ResponseBody Iterable<Project> getAllProjects() {
		return projectRepository.findAll();
	}
	
	@GetMapping("/categories")
	public @ResponseBody Collection<CategoryPojo> getAllCategories() {
		return categoryService.findAllRoot();
	}
	
	@GetMapping("/roles")
	public @ResponseBody Collection<RolePojo> getAllRoles() {
		return roleService.findAll();
	}
	
	@GetMapping("/units")
	public @ResponseBody Collection<UnitPojo> getAllUnits() {
		return unitService.findAll();
	}
	
	@GetMapping("/hcards")
	public @ResponseBody Iterable<Hcard> getAllHcards() {
		return hcardRepository.findAll();
	}
}


