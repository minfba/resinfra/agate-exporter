package de.uniba.minf.agate.exporter.repository;

import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {

}
