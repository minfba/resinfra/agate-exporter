package de.uniba.minf.agate.exporter.model;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tx_academy_domain_model_relations")
public class Relation {
	
	@Id
	@Column(name="uid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer uid;

	@Column(name="t3_origuid")
	private Integer origId;
	
	@JsonIgnore
	@Column(name="sys_language_uid")
	private Integer languageId;
	
	@Column(name="persistent_identifier")
	private String persistentIdentifier;
	
	private int type;

	
	private int project;
	
	@ManyToOne
    @JoinColumn(name="unit", nullable=true)
	private Unit unit;
	
	@ManyToOne
    @JoinColumn(name="person", nullable=true)
	private Person person;
	
	@ManyToOne
    @JoinColumn(name="hcard", nullable=true)
	private Hcard hcard;
	
	@ManyToOne
    @JoinColumn(name="role", nullable=true)
	private Role role;
}
