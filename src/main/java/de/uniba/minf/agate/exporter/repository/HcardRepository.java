package de.uniba.minf.agate.exporter.repository;

import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Hcard;

public interface HcardRepository extends CrudRepository<Hcard, Integer> {

}
