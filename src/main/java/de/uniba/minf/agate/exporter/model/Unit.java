package de.uniba.minf.agate.exporter.model;

import de.uniba.minf.agate.exporter.model.base.BaseLocalized;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "tx_academy_domain_model_units")
public class Unit extends BaseLocalized {
	private String description;
	private String acronym;
}
