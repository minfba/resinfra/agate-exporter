package de.uniba.minf.agate.exporter.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import de.uniba.minf.agate.exporter.model.Relation;

public interface RelationRepository extends CrudRepository<Relation, Integer> {
	public Set<Relation> findByProject(int project);
}
