package de.uniba.minf.agate.exporter.pojo.base;

import java.util.ArrayList;
import java.util.List;

import de.uniba.minf.agate.exporter.pojo.LocalizedDescriptionPojo;
import lombok.Data;

@Data
public class BaseLocalizedPojo {
	private List<Integer> ids = new ArrayList<>();
	private List<LocalizedDescriptionPojo> descriptions = new ArrayList<>();
	private String persistentIdentifier;
	
	public void addIdIfNotContained(int id) {
		if (!ids.contains(id)) {
			ids.add(id);
		}
	}
}
