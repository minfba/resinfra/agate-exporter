package de.uniba.minf.agate.exporter.pojo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.uniba.minf.agate.exporter.pojo.base.BaseLocalizedPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CategoryPojo extends BaseLocalizedPojo {
	@JsonIgnore
	private CategoryPojo parent;
	private List<CategoryPojo> children = new ArrayList<>();
}
