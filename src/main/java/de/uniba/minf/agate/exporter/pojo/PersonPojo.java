package de.uniba.minf.agate.exporter.pojo;

import lombok.Data;

@Data
public class PersonPojo {
	public enum GENDER { MALE, FEMALE }
	
	private String persistentIdentifier;
	
	private Integer id;
	private String givenName;
	private String additionalName;
	private String familyName;
	private String prefix;
	private String suffix;
	private GENDER gender;
}
